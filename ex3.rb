# Prints a message before we start counting.
puts "I will now count my chickens:"

# Ruby evaluates arithmetic expressions first with the operator of highest
# precedence. Operators of equal precedence are evualates leftmost-first.  `*`,
# `/`, `%` are of equal precendence. The are of higher precendence than `+`,
# `-`, which are both of equal precendence.

# First divides (5), then adds.
puts "Hens #{25.0 + 30.0 / 6.0}"
# Multiplies (75), mods (3), subtracts.
puts "Roosters #{100.0 - 25.0 * 3.0 % 4.0}"

# Prints a message before more abstract questions.
puts "Now I will count the eggs:"

# Mods (0), divides (0), then adds (7). `/` is integer division without
# remainder when applied to integers; when applied to floats it gives the fration. Originally this program used integers, but in the study drill we changed it to use floating point numbers to make the answer more accurate. 6.75 eggs!?
puts 3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0

# Just prints the string. Ruby does not interpret the arithmetic here because
# it is not in hashy brackets.
puts "Is it true that 3 + 2 < 5 - 7?"

# This returns a boolean expression because of the less-than sign. Less-than
# has lower precedence than arithmetic.
puts 3.0 + 2.0 < 5.0 - 7.0

# Same as the chickens, print and do the sums.
puts "What is 3 + 2? #{3.0 + 2.0}"
puts "What is 5 - 7? #{5.0 - 7.0}"

# Revelation
puts "Oh, that's why it's false."

# Glutton for punishment
puts "How about some more."

# We haven't learned about variables yet.
puts "Is it greater? #{5.0 > -2.0}"
puts "Is it greater or equal? #{5.0 >= -2.0}"
puts "Is it less or equal? #{5.0 <= -2.0}"

