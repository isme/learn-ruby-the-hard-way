# Write a script similar to the last exercise that uses `read` and `argv` to read the file you just created.

filename = ARGV.first

puts "Opening the file..."
source = open(filename, 'r')

puts "I'm going to read the file."
puts "Content of #{filename}: "
print source.read

puts "Finally, close the file."
source.close

