# Here's some new strange stuff, remember type it exactly.

# Declare a string. Nothing special here syntactically. The string contains
# the abbreviated names of the days of the week separated by spaces.
days = "Mon Tue Wed Thu Fri Sat Sun"
# Declare another string containing the abbreviated names of the months of the
# year. This time the names are separated by new lines. To put a new line in a
# sting you need to type `\n`. Ruby converts this sequence to a new line when
# it prints the string.
months = "Jan\nFeb\nMar\nApr\nMay\nJun\nJul\nAug"

# Print the days of the week on one line.
puts "Here are the days: #{days}"
# Print the months of the year. Each month is on a different line because of
# the new lines embedded in the string. The first month is on the same line as
# the intro text because there is no new line before it.
puts "Here are the months: #{months}"

# Prints another string on multiple lines. This time the new lines are
# literally inside the string, so you can see them in the code. Normally the
# qoute characters that mark the start and the end of a string have to be on
# the same line, but if the string starts with three quotes, then everything
# after is part of the string until the next three quotes in a row.
puts """
There's something going on here.
With the three double-quotes.
We'll be able to type as much as we like.
Even 4 lines if we want, or 5, or 6.
"""
