# Take the first argument from the command line as the name of a file to read.
input_file = ARGV.first

# define function called print_all which takes a single argument called f.
def print_all(f)
  # Call the read function on the object, and print what it returns.
  puts f.read
# End the function. What follows is not part of the function.
end

# define a function called rewind which takes a single argument called f.
def rewind(f)
  # Reset the file's position to the beginning (position 0).
  f.seek(0)
end

# This function takes two arguments and is short but actually does quite a lot.
# Basically it prints a line. The content of the line is first whatever is in
# the first parameter, seperated by a comma and a space, and then part of the
# file. Calling gets on the file gets the next line, including the line ending
# character. Calling chomp on that removes the line ending character. Puts
# actually writes another line ending character after all the that, so in the
# end there is still one newline character.
def print_a_line(line_count, f)
  puts "#{line_count}, #{f.gets.chomp}"
end

# Okay, now we get to work. Open the file (starts at position 0).
current_file = open(input_file)

# Tell me that I'm about to print the whole file at once.
puts "First let's print the whole file:\n"

# Call the print_all function on the file to do it.
print_all(current_file)

# Now tell me that we're going back to the start of the file.
puts "Now let's rewind, kind of like a tape."

# Call the rewind function on the file to do it.
rewind(current_file)

# Now tell me that we're going to print not the whole file, but just three
# lines. It so happens that the file might only have three lines, or less. What
# happens if there are less than three lines in the file?
puts "Let's print three lines:"

# Set the line counter to 1, for the first line. Not like the file position
# counter, which starts at 0. Then call print_a_line to print that count of 1
# and take a line from the file, starting at the beginning.
current_line = 1
print_a_line(current_line, current_file)

# Increment the line counter and call the function again to print 2 and the
# next line.
current_line += 1
print_a_line(current_line, current_file)

# This art is the same as before, but will print the third line because the
# file object and the line count are different when we start.
current_line += 1
print_a_line(current_line, current_file)

# It turns out that if there are less than 2 lines in the file then the last
# line prints an error instead because gets returns nothing, and you can't call
# a function like chomp on nothing.
# 
# Let's print three lines:
# 1, line 1
# 2, line 2
# ex20.rb:25:in `print_a_line': undefined method `chomp' for nil:NilClass (NoMethodError)
# 	from ex20.rb:62:in `<main>'

