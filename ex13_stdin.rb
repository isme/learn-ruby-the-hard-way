date, time = ARGV

# You need to use `$stdin` to get input if you mix `gets.chomp` with `ARGV`.
# Otherwise, Ruby tries to read from a file with the name of the first
# command-line argument, which will probably fail like this.
#
# ```
# $ ruby ex13_stdin.rb tomorrow allnight 
# Venue: ex13_stdin.rb:3:in `gets': No such file or directory - tomorrow (Errno::ENOENT)
# 	from ex13_stdin.rb:3:in `gets'
# 	from ex13_stdin.rb:3:in `<main>'
# ```
print "Venue: "
venue = $stdin.gets.chomp
print "Cover: "
cover = $stdin.gets.chomp
print "Group: "
group = $stdin.gets.chomp

puts """
Date: #{date}
Time: #{time}
Venue: #{venue}
Cover: #{cover}
Group: #{group}
"""

