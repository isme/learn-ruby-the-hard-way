# Declares a format string. A format string contains formatting instructions
# using the `%{}` syntax. Format strings are not usually printed literally, but
# are combined with the format operator `%` to provide values for the
# variables. The values come from a hash on the right side of the operator.
# The name inside the brackets of the format string refers to a key of the
# hash, whose value should be interpolated at that point in the string.
# In this case, the values of keys `first`, `second`, `third`, and `fourth` are
# printed in order, each separated by a space.
formatter = "%{first} %{second} %{third} %{fourth}"

# Print a formatted string using integers as hash values.
puts formatter % {first: 1, second: 2, third: 3, fourth: 4}
# Print a formatted string using strings as hash values.
puts formatter % {first: "one", second: "two", third: "three", fourth: "four"}
# Print a formatted string using booleans as hash values.
puts formatter % {first: true, second: false, third: true, fourth: false}
# Print a formatted string using the format string itself as input. This is
# weird.
puts formatter % {first: formatter, second: formatter, third: formatter, fourth: formatter}

# Print a formatted string using lines of a rhyme as input.
# Each key-value pair of the hash is on a new line to make it easier to read.
puts formatter % {
  first: "I had this thing.",
  second: "That you could type up right.",
  third: "But it didn't sing.",
  fourth: "So I said goodnight."
}

