# The first and third puts lines here just print a literal string, nothing
# special. The second puts line interpolates the another literal into the
# string. It is effectively the same as just putting those characters in the
# string first, except that Ruby does more work internally to construct a
# bigger string from temporary literals. The fourth puts line prints ten dots.
# When the multiplication operator has a string on the left, it returns a copy
# of that string joined to itself as many times as the number on the right.
puts "Mary had a little lamb."
puts "Its fleece was white as #{'snow'}."
puts "And everywhere that Mary went."
puts "." * 10  # what'd that do?

# Each of these statements names a string one character long.
# This allows each character to be addressed individually, but is very verbose.
# We haven't learned about arrays yet.
end1 = "C"
end2 = "h"
end3 = "e"
end4 = "e"
end5 = "s"
end6 = "e"
end7 = "B"
end8 = "u"
end9 = "r"
end10 = "g"
end11 = "e"
end12 = "r"

# watch that print vs. puts on this line what's it do?
# print is like puts, but it doesn't finish the line. The next output starts on
# the same line. puts always finishes the line so that the next outputs starts
# on a new line.
print end1 + end2 + end3 + end4 + end5 + end6
puts end7 + end8 + end9 + end10 + end11 + end12

