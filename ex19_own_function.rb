def draw_shape(fill, width, height)
  puts ("#{fill}" * width + "\n") * height + "\n"
end

# Cool, a rectangle!
draw_shape("#", 4, 3)
# ALT+J
draw_shape("∆", 5, 5)
# Oveido nights
draw_shape("FAIK", 3, 10)
# Why does this only print a 2x2 and lots of blanks?
# because it doesn't return a value!
draw_shape(draw_shape(".", 2, 2), 2, 2)
# Maybe we can fix that, but let's learn more about functions first.
