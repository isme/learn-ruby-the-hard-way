# Learn Ruby the hard way

Solutions to Zed Shaw's "[Learn Ruby the hard way](http://learnrubythehardway.org/book/)".

The final files may not produce the same solutions as those in the book because of modifications in the study drills. Check the file history for earlier versions.
