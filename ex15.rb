# Read the first argument from the command line and store it as the file name.
# This way is better for passing the file name if you need to run the script
# from lots of different other programs, especially when they can't send the
# name to stdin of ruby when it is running this script.
filename = ARGV.first

# Open the named file and hold on to the file object.
txt = open(filename)

# Print out a message explain that what comes next is the content of said file.
puts "Here's your file #{filename}:"
# Print out the content of the file, using the read method of the file object.
print txt.read

# Close the file.
txt.close

# Print a message to prompt for another file name.
# This way is better for getting the file name if you don't know what it will
# be when ruby starts, for example if you are making an interactive console
# program where lots of files will be opened over time (vi for example).
print "Type the filename again: "
# Get another file name, this time from standard input.
file_again = $stdin.gets.chomp

# Open the other named file and hold on to the file object.
txt_again = open(file_again)

# Print out the content of the other file.
print txt_again.read

# Close the other file.
txt_again.close

