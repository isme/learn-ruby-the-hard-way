# Define the variable and set it to an integer.
types_of_people = 10
# The number 10 is converted to a string and embedded in this string,
x = 'There are #{types_of_people} types of people.'
# Just another string.
binary = 'binary'
# Another string.
do_not = 'don\'t'
# The previous two strings are copied into this bigger string.
y = 'Those who know #{binary} and those who #{do_not}.'

# Print x.
puts x
# Print y.
puts y

# Print x again, but make it part of a bigger string first.
puts 'I said: #{x}.'
# Print y again, but make it part of an even bigger string first.
puts 'I also said: '#{y}'.'

# Define a boolean variable.
hilarious = false
# The boolean is converted to a string before being embedded in the string.
joke_evaluation = 'Isn\'t that joke so funny?! #{hilarious}'

# Print the boolean evaluation.
puts joke_evaluation

# More strings.
w = 'This is the left side of...'
e = 'a string with a right side.'

# Join the two strings together and print them.
# Here `+` is a string concatenation operator. It takes a copy of each string
# and joins them together to make a new one.
puts w + e

# If you change all the strings to use single quotes instead of double quotes,
# the program stops working. The first problem is that some of the strings
# contains a single quote character, so ruby thinks that the following
# character refers to a undefined name.
#
# ```
# ex6.rb:8: syntax error, unexpected tIDENTIFIER, expecting end-of-input
# do_not = 'don't'
# ```
# You can escape the single quotes inside the string with a slash (`\`) to tell Ruby that the quote is part of the string instead of the end marker. The slash does not appear in the string.
#
# The next bigger problem is that the hashy brackets are no longer replaced with named values. Instead the characters are printed literally.
