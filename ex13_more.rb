people, beds, houses, streets, cities = ARGV
puts "There are about #{people} people in each bed."
puts "There are about #{beds} beds in each house."
puts "There are about #{houses} houses in each street."
puts "There are about #{streets} streets in each city."
puts "There are about #{cities} cities in each country."
population = people.to_i * beds.to_i * houses.to_i * streets.to_i * cities.to_i
puts "So there are about #{population} people in each country."

