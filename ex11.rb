# gets.chomp is actually two commands. The first, `gets`, waits for you to type
# something. When you type enter, it passes what you typed to the program as a
# string including a new line character at the end. `chomp` is a function of
# the string object that returns a new string without the newline character at
# the end. The string without the new line at the end is what gets assigned to
# each variable.
print "How old are you? "
age = gets.chomp
print "How tall are you? "
height = gets.chomp
print "How much do you weigh? "
weight = gets.chomp

puts "So, you're #{age} old, #{height} tall and #{weight} heavy."

puts "Another form:"
print "What did you have for breakfast today? "
breakfast = gets.chomp
print "Where would you rather be? "
wish_you_were_here = gets.chomp
print "Would you like a beer? "
thirsty = gets.chomp

puts "A ver, comiste #{breakfast}, preferiría estar #{wish_you_were_here}, y dices #{thirsty} a la cerveza."

