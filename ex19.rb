# This line defines a function called cheese_and_crackers and makes it expect
# two parameters.
def cheese_and_crackers(cheese_count, boxes_of_crackers)
  # This line prints the number of cheeses using the first parameter.
  puts "You have #{cheese_count} cheeses!"
  # This line prints the number of crackers using the second parameter.
  puts "You have #{boxes_of_crackers} boxes of crackers!"
  # The next lines just print some text.
  puts "Man that's enough for a party!"
  puts "Get a blanket.\n"
# This line ends the function definition. What follows is no longer part of the
# function.
end

# Call the function once with some literal numbers.
puts "We can just give the function numbers directly:"
cheese_and_crackers(20, 30)

# Define some variables with literal numbers.
puts "OR, we can use variables from our script:"
amount_of_cheese = 10
amount_of_crackers = 50

# Call the function again with the variables.
cheese_and_crackers(amount_of_cheese, amount_of_crackers)

# Call the function again with expressions which evaluate to numbers.
puts "We can even do math inside too:"
cheese_and_crackers(10 + 20, 5 + 6)

# Call the function with expressions mixed of variables and literals.
puts "And we can combine the two, variables and math:"
cheese_and_crackers(amount_of_cheese + 100, amount_of_crackers + 1000)

